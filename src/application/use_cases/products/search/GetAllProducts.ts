import ProductsRepository from '../../../../domain/contexts/products/ProductsRepository';
export default class GetAllProducts {
  constructor(private resposity: ProductsRepository) {
    this.run = this.run.bind(this);
  }

  public async run() {
    return this.resposity.getAll();
  };
}
