import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('Productos')
export default class ProductEntity {
  @PrimaryColumn()
  Cod_Producto: string;

  @Column()
  Nom_Producto: string;

  constructor(Cod_Producto: string, Nom_Producto: string) {
    this.Cod_Producto = Cod_Producto
    this.Nom_Producto = Nom_Producto
  }
}
