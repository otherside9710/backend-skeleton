import { IProducts } from '../../interfaces/IProducts';

export default interface ProductsRepository {
  getAll(): Promise<IProducts[] | never>;
}
