import { Column, ViewEntity } from 'typeorm';

@ViewEntity({ name: 'vw_api_CambioVendedor' })
export default class VendorsEntity {
  @Column({ type: 'smallint', nullable: false })
  Cod_Empresa: number;

  @Column({ type: 'smallint', nullable: false })
  Cod_Vendedor: number;

  @Column({ type: 'nvarchar', nullable: false })
  Nom_Vendedor: string;

  @Column({ type: 'nvarchar', nullable: false })
  Tel1_Usuario: string;

  @Column({ type: 'nvarchar', nullable: false })
  Email_Usuario: string;

  @Column({ type: 'bigint', nullable: false })
  id_control_cambio_datos: number;

  @Column({ type: 'char', nullable: false })
  Cod_tipo_Cambio: string;

  @Column({ type: 'char', nullable: false })
  Cod_Marca_Estado_Cambio: string;

  @Column({ type: 'char', nullable: true })
  Identificador_Magento: string;

  constructor(
    Cod_Empresa: number,
    Cod_Vendedor: number,
    Nom_Vendedor: string,
    Tel1_Usuario: string,
    Email_Usuario: string,
    id_control_cambio_datos: number,
    Cod_tipo_Cambio: string,
    Cod_Marca_Estado_Cambio: string,
    Identificador_Magento: string,
) {
    this.Cod_Empresa = Cod_Empresa
    this.Cod_Vendedor = Cod_Vendedor
    this.Nom_Vendedor = Nom_Vendedor
    this.Tel1_Usuario = Tel1_Usuario
    this.Email_Usuario = Email_Usuario
    this.id_control_cambio_datos = id_control_cambio_datos
    this.Cod_tipo_Cambio = Cod_tipo_Cambio
    this.Cod_Marca_Estado_Cambio = Cod_Marca_Estado_Cambio
    this.Identificador_Magento = Identificador_Magento
  }
}
