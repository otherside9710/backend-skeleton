import { IVendors } from '../../interfaces/IVendors';

export default interface VendorsRepository {
  getAll(): Promise<IVendors[] | never>;
}
