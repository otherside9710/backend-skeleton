import StatusCodes from 'http-status-codes';
import { BaseError } from '../shared/BaseError';

export class ProductsBadRequestError extends BaseError {
  constructor(
      message: string,
      status: number = StatusCodes.BAD_REQUEST,
      info: { error: string } = {error: 'invalid params for search products'},
  ) {
    super(message, status, info);
  }
}
