export interface IVendors {
  Cod_Empresa: number;
  Cod_Vendedor: number;
  Nom_Vendedor: string;
  Tel1_Usuario: string;
  Email_Usuario: string;
  id_control_cambio_datos: number;
  Cod_tipo_Cambio: string;
  Cod_Marca_Estado_Cambio: string;
  Identificador_Magento: string;
}
