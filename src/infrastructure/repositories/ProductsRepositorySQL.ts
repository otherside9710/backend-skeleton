import { IProducts } from '../../domain/interfaces/IProducts';
import SQLRepository from '../orm/SQL/SQLRepository';
import ProductEntity from '../../domain/contexts/products/ProductsEntity';
import ProductsRepository from '../../domain/contexts/products/ProductsRepository';
import { ProductsBadRequestError } from '../../domain/errors/products/ProductsBadRequestError';

export default class ProductsRepositorySQL extends SQLRepository implements ProductsRepository {
  public async getAll(): Promise<IProducts[] | never> {
    try {
      return this.getAllSQL(ProductEntity);
    } catch (error: any) {
      throw new ProductsBadRequestError('Error on request to vtex');
    }
  }
}
