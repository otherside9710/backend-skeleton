import {ContainerBuilder} from 'node-dependency-injection';
import ProductsRepositorySQL from '../../repositories/ProductsRepositorySQL';
import GetAllProducts from '../../../application/use_cases/products/search/GetAllProducts';
import GetProductsController from '../../controllers/products/search/GetProductsController';

const productsContainer = new ContainerBuilder();

// -repositories
productsContainer.register('products.repositories.ProductsRepositorySQL', ProductsRepositorySQL);

// -use cases
productsContainer
    .register('products.useCases.search.GetAllProducts', GetAllProducts)
    .addArgument('products.repositories.ProductsRepositorySQL');

// -controllers
productsContainer
    .register('products.controller.search.GetProductsController', GetProductsController)
    .addArgument('products.useCases.search.GetAllProducts');

export default productsContainer;
