import { NextFunction, Request, Response } from 'express';
import { BaseController } from '../../BaseController';
import GetAllProducts from '../../../../application/use_cases/products/search/GetAllProducts';
import { StatusCodes } from 'http-status-codes';
import ProductsRepository from '../../../../domain/contexts/products/ProductsRepository';
import productsContainer from '../../../dpi/products';

export default class GetProductsController implements BaseController {
  private useCase: GetAllProducts;
  private repository: ProductsRepository;

  constructor() {
    this.repository = productsContainer.get('products.repositories.ProductsRepositorySQL');
    this.useCase = new GetAllProducts(this.repository);
    this.run = this.run.bind(this);
  }

  public async run(_req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.useCase.run();
      res.status(StatusCodes.OK).send(data);
    } catch (error: any) {
      next(error);
    }
  }
}
