import { AppDataSource } from './SQLDataSource';

export default class SQLRepository {
  constructor() {
    this.getAllSQL.bind(this);
  }

  public async getAllSQL(entity: any): Promise<any> {
    const repository = AppDataSource.getRepository(entity)
    return repository.find();
  }
}
