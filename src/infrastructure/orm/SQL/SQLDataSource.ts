import 'reflect-metadata'
import { DataSource } from 'typeorm'
import Products from '../../../domain/contexts/products/ProductsEntity'
import Vendors from '../../../domain/contexts/vendors/VendorsEntity'
import environment from '../../config'

export const AppDataSource = new DataSource({
  type: 'mssql',
  host: environment.BD_HOST,
  port: environment.BD_PORT,
  username: environment.BD_USER,
  password: environment.BD_PASS,
  database: environment.BD_NAME,
  entities: [Products, Vendors],
  synchronize: false,
  logging: false,
  extra: {
    trustServerCertificate: true,
  },
})
