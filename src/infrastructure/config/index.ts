import 'dotenv/config';
const env = (key: string) => {
  return process.env[key]
};

export default {
  PORT: env('PORT') ?? 3000,
  NODE_ENV: env('NODE_ENV') ?? 'dev',
  BD_HOST: env('BD_HOST'),
  BD_PORT: parseInt(env('BD_PORT') as string),
  BD_USER: env('BD_USER'),
  BD_PASS: env('BD_PASS'),
  BD_NAME: env('BD_NAME'),
}
