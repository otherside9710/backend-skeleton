/* eslint-disable no-console */
import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import {hostname} from 'os';
import {ErrorHandler} from '../errors/ErrorHandler';
import * as http from 'http';
import { BaseRoutes } from '../routes/BaseRoutes';

export class Server {
  private readonly port: number;
  private httpServer?: http.Server;
  private baseRoutes = new BaseRoutes();
  public app = express();

  constructor(port: number) {
    this.port = port;
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}));
    this.app.use(helmet.xssFilter());
    this.app.use(helmet.noSniff());
    this.app.use(helmet.hidePoweredBy());
    this.app.use(helmet.frameguard({action: 'deny'}));
    this.app.use('/emassa/api', this.baseRoutes.router);
    this.app.use(ErrorHandler);
  }

  listen = async (): Promise<void> => {
    return new Promise((resolve) => {
      this.httpServer = this.app.listen(this.port, () => {
        console.info(`Server ready in: ${hostname}:${this.port}`);
        resolve();
      });
    });
  };

  getHTTPServer = () => {
    return this.httpServer;
  }
}
