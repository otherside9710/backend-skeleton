import { AppDataSource } from '../orm/SQL/SQLDataSource';
import {Run} from './Run';

try {
  new Run().start().then();
  AppDataSource.initialize();
} catch (error: any) {
  // eslint-disable-next-line no-console
  console.error(error);
  process.exit(1);
}

process.on('uncaughtException', (err) => {
  // eslint-disable-next-line no-console
  console.error(err);
  process.exit(1);
});
