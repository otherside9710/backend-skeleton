import {NextFunction, Response, Request} from 'express';
import {BaseError} from './BaseError';

export const ErrorHandler = (error: Error, req: Request, res: Response, next: NextFunction): Response => {
  printError(error);
  if (error instanceof BaseError) return res.status(error.status).send(error.info);
  return res.status(500).send();
};

const printError = (error: Error): void => {
   // eslint-disable-next-line no-console
   console.error(error);
};
