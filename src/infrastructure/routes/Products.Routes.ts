import {Router} from 'express';
import GetProductsController from '../controllers/products/search/GetProductsController';

export default class ProductsRoutes {
  constructor(readonly router = Router()) {
    const getProductsController: GetProductsController = new GetProductsController();
    router.get('/', getProductsController.run);
  }
}
