import {Router} from 'express';
import ProductRoutes from './Products.Routes';
export class BaseRoutes {
  constructor(readonly router = Router(), private readonly productRoutes = new ProductRoutes()) {
    router.use('/products', this.productRoutes.router);
  }
}
